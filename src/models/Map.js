const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const mapSchema = new Schema({
  title: String,
  createDate: { type: Date, default: Date.now },
  tags: [{ type: Schema.Types.ObjectId, ref: 'Tag' }],
  creator: { type: Schema.Types.ObjectId, ref: 'User' }
});

const Map = mongoose.model('Map', mapSchema);

export default Map;
