const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const tagSchema = new Schema({
  title: String,
  pid: { type: Schema.Types.ObjectId, ref: 'Node' },
  createDate: { type: Date, default: Date.now },
  map: { type: Schema.Types.ObjectId, ref: 'Map' },
  creator: { type: Schema.Types.ObjectId, ref: 'User' }
});

const Note = mongoose.model('Node', tagSchema);

export default Note;
