const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const categorySchema = new Schema({
  title: String,
  createDate: { type: Date, default: Date.now },
  creator: { type: Schema.Types.ObjectId, ref: 'User' }
});

const Note = mongoose.model('Category', categorySchema);

export default Note;
