const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const phraseSchema = new Schema({
  phrase: String,
  definition: String,
  example: String,
  createDate: { type: Date, default: Date.now },
  tags: [{ type: Schema.Types.ObjectId, ref: 'Tag' }],
  creator: { type: Schema.Types.ObjectId, ref: 'User' }
});

const Note = mongoose.model('Phrase', phraseSchema);

export default Note;
