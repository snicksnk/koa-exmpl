const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const tagSchema = new Schema({
  name: String,
  description: String,
  createDate: { type: Date, default: Date.now },
  categories: [{ type: Schema.Types.ObjectId, ref: 'Category' }],
  creator: { type: Schema.Types.ObjectId, ref: 'User' }
});

const Note = mongoose.model('Tag', tagSchema);

export default Note;
