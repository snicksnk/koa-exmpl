import Map from '../../models/Map';
import Node from '../../models/Node';

export async function createMap(ctx) {
  const { user } = ctx.state;

  // TODO Add validation
  const mapFields = {
    ...ctx.request.smartParams,
    creator: user._id
  };

  const map = new Map(mapFields);
  await map.save();
  ctx.body = { map };
}

export async function getMaps(ctx) {
  const { user } = ctx.state;
  const maps = await Map.find({ creator: user._id });
  ctx.body = { maps };
}

export async function getMap(ctx, next) {
  // TODO add id validator
  const { user } = ctx.state;
  const { id } = ctx.request.smartParams;
  const map = await Map.findById(id);
  const nodes = await Node.find({ map: id });

  if (!map) {
    ctx.throw(404);
  }

  if (!map.creator.equals(user._id)) {
    ctx.throw(403);
  }

  ctx.body = {
    map: { ...map, nodes }
  };

  if (next) {
    return next();
  }
}

export async function updateMap(ctx) {
  const { map } = ctx.body;
  const mapFields = {
    ...ctx.request.smartParams,
  };

  Object.assign(map, mapFields);
  await map.save();
  ctx.body = {
    map
  };
}

export async function deleteMap(ctx) {
  const map = ctx.body.map;
  await map.remove();

  ctx.status = 200;
  ctx.body = {
    success: true
  };
}
