import { ensureUser } from '../../middleware/validators';
import { prepareParams, extract } from '../../middleware/smartRequest';

import * as map from './controller';

export const baseUrl = '/api/v1/maps';

export default [
  {
    method: 'POST',
    route: '/',
    handlers: [
      ensureUser,
      prepareParams(ctx => extract(ctx.request.body)(['title', 'definition', 'example', 'tags'])),
      map.createMap
    ]
  },
  {
    method: 'GET',
    route: '/',
    handlers: [
      ensureUser,
      map.getMaps
    ]
  },
  {
    method: 'GET',
    route: '/:id',
    handlers: [
      ensureUser,
      prepareParams(ctx => extract(ctx.params)(['id'])),
      map.getMap
    ]
  },
  {
    method: 'PUT',
    route: '/:id',
    handlers: [
      ensureUser,
      prepareParams(ctx => ({
        ...extract(ctx.params)(['id']),
        ...extract(ctx.request.body)(['title', 'definition', 'example', 'tags']),
      })),
      map.getMap,
      map.updateMap
    ]
  },
  {
    method: 'DELETE',
    route: '/:id',
    handlers: [
      ensureUser,
      prepareParams(ctx => extract(ctx.params)(['id'])),
      map.getMap,
      map.deleteMap
    ]
  }
];
