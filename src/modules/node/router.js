import { ensureUser } from '../../middleware/validators';
import { prepareParams, extract } from '../../middleware/smartRequest';

import * as node from './controller';

export const baseUrl = '/api/v1/nodes';

export default [
  {
    method: 'POST',
    route: '/',
    handlers: [
      ensureUser,
      prepareParams(ctx => extract(ctx.request.body)(['title', 'pid', 'map'])),
      node.createNode
    ]
  },
  {
    method: 'GET',
    route: '/',
    handlers: [
      ensureUser,
      node.getNodes
    ]
  },
  {
    method: 'GET',
    route: '/:id',
    handlers: [
      ensureUser,
      prepareParams(ctx => extract(ctx.params)(['id'])),
      node.getNode
    ]
  },
  {
    method: 'PUT',
    route: '/:id',
    handlers: [
      ensureUser,
      prepareParams(ctx => ({
        ...extract(ctx.params)(['id']),
        ...extract(ctx.request.body)(['title', 'pid', 'map']),
      })),
      node.getNode,
      node.updateNode
    ]
  },
  {
    method: 'DELETE',
    route: '/:id',
    handlers: [
      ensureUser,
      prepareParams(ctx => extract(ctx.params)(['id'])),
      node.getNode,
      node.deleteNode
    ]
  }
];
