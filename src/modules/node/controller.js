import Node from '../../models/Node';

// TODO Add checck for one root node
export async function createNode(ctx) {
  const { user } = ctx.state;

  // TODO Add validation
  const nodeFields = {
    ...ctx.request.smartParams,
    creator: user._id
  };

  const node = new Node(nodeFields);
  await node.save();
  ctx.body = { node };
}

export async function getNodes(ctx) {
  const { user } = ctx.state;
  const nodes = await Node.find({ creator: user._id }).populate('categories');
  ctx.body = { nodes };
}

export async function getNode(ctx, next) {
  // TODO add id validator
  const { user } = ctx.state;
  const { id } = ctx.request.smartParams;
  const node = await Node.findById(id);

  if (!node) {
    ctx.throw(404);
  }

  if (!node.creator.equals(user._id)) {
    ctx.throw(403);
  }

  ctx.body = {
    node
  };

  if (next) {
    return next();
  }
}

export async function updateNode(ctx) {
  const { node } = ctx.body;
  const nodeFields = {
    ...ctx.request.smartParams,
  };

  Object.assign(node, nodeFields);
  await node.save();
  ctx.body = {
    node
  };
}

export async function deleteNode(ctx) {
  const node = ctx.body.node;
  await node.remove();

  ctx.status = 200;
  ctx.body = {
    success: true
  };
}
